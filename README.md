[[_TOC_]]

## Communication between iFrame and host

### Same origin

Use [Broadcast Channel API](https://developer.mozilla.org/en-US/docs/Web/API/Broadcast_Channel_API)

### Different origins

Use [`Window.postMessage()`](https://developer.mozilla.org/en-US/docs/Web/API/Window/postMessage)

:warning: Always provide target origin and make get familiar with security concerns metioned on the page above